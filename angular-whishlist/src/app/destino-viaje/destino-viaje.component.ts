import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  //creacion de una variable en el componente
  @Input() destino : DestinoViaje;
  //Envolver el componente con una clase
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {  }

  ngOnInit(): void {
  }

}
